# Boilerplate (BP)

This repo is set up as a boilerplate for develop and deployment of rest API and SPA for using that API

## Offers
- API with JWT and token protection
- User role architecture with built in claims system
- Email service supporting email threading
- SPA written in Vue.js
- Front-end methods for login, login with google, password reset and menu rendered depending on user claims
- Vue.js components for modal, buttons, input, checkbox (supports indeterminate state)
- ...and much more

## Dependencies
- Sends email using mailgun
- Offers Google Oauth for login
- Uses Google Recaptcha for password reset
- Deployment is done using bitbucket pipelines (see `bitbucket-pipleins.yml`)
- App is set to be hosted on AWS (S3 and LAMBDA)

## Environments
App environment is defined by the .env file or using process environment variables.

## Install
Run `yarn` or `NPM install` to install all dependencies. Preferably you should run this in node > 12.8.

Local development uses SQLite so no DB server is needed.

## Develop
- Copy .env.example to .env and update variables accordingly (keep `ENV` set to `local` to run)
- `yarn cli migrate up` to migrate SQLite DB (will create)
- Run `yarn watch` to serve with hot reloading

## AWS
Setup for AWS to run the app
### Lambda
The app is set to run on AWS Lambda.
- Create a new app with three aliases (`development`, `staging` and `production`)
- Add environment variables to the app. Copy all variables from the `.env.example`-file. Sadly Lambda doesn't support alias specific environment variables so each variable needs to appear three times for each environment and be prefixed either: `DEVELOPMENT_`, `STAGING_` or `PRODUCTION_`.
- Create two test scenarios for migration up and down containing `{ "action": "up" }` and `{ "action": "down" }`. Running these test scenarios over an alias makes sure migration is running for the correct DB (given env's are set correctly)

### S3
Create thre buckets servered as static websites for the SPA

### API Gateway
- Create and API Gateway with three stages.
- Add variable to each stage named `lambdaAlias` and set value to each environment lambda alias created earlier
- Create a proxy resource to the Lambda function. Set "Lambda Function" to `bp:${stageVariables.lambdaAlias}`. This helps the API to direct request to correct lambda alias depending on stage
- Use Custom Domain Names for API Gateway to direct different domains to different stages

### Custom domains
Do put API and SPA under custom domain names you will need to use CloudFront and Certificates.