// Libs
import { RouterModule } from "@agilearchitects/server";
import { AuthController } from "./controllers/auth.controller";
import { MiddlewareController } from "./controllers/middleware.controller";
import { RouterController } from "./controllers/router.controller";
import { UserController } from "./controllers/user.controller";
import { VersionController } from "./controllers/version.controller";

export const router = (
  middlewareController: MiddlewareController,
  versionController: VersionController,
  authController: AuthController,
  routerController: RouterController,
  userController: UserController,
): RouterModule => {
  // Create router
  const router: RouterModule = new RouterModule();

  router.get("version", { name: "version" }, versionController.index());
  router.get("user", { name: "user" }, middlewareController.token(), userController.index());

  router.group("auth", { name: "auth" }, [], (router: RouterModule) => {
    router.post("login", {
      name: "login",
      document: {
        inputInterface: {
          name: "ILoginDTO",
          path: "./src/shared/dto/login.dto.ts"
        },
        outputInterface: {
          name: "ILoginPayloadDTO",
          path: "./node_modules/@agilearchitects/authenticaton/lib/dto/login-payload.dto.d.ts"
        }
      }
    }, authController.login());
    // Route for requesting password reset
    router.post("reset-password", {},
      middlewareController.reCaptchaToken(),
      authController.requestPasswordReset()
    );

    // Route for validating password reset token
    router.post("validate-reset-token", {}, authController.validateResetToken());

    // Route for resetting password
    router.post("password-reset", {}, authController.resetPassword());

    router.post("create", {}, middlewareController.token(), authController.create());
    return router;
  });

  router.get("router", { name: "router" }, routerController.index(router));

  return router;
}