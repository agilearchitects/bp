// Libs
import { BannedTokenService, ClaimService, GroupService, UserService } from "@agilearchitects/authenticaton";
import { EnvService } from "@agilearchitects/env";
import { HashtiService } from "@agilearchitects/hashti";
import { JWTService } from "@agilearchitects/jwt";
import { LogModule } from "@agilearchitects/logmodule";
import { bodyParse, cors, handlerMethod, vhost } from "@agilearchitects/server";
import { logHandler } from "@agilearchitects/typeorm-helper";
import * as typeorm from "typeorm";

// Modules
import { PackageJsonReaderModule } from "../modules/package-json-reader.module";

// Entities
import { MiddlewareController } from "../controllers/middleware.controller";
import { BannedTokenEntity } from "../entities/banned-token.entity";
import { ClaimEntity } from "../entities/claim.entity";
import { GroupEntity } from "../entities/group.entity";
import { UserEntity } from "../entities/user.entity";

// Factories
import { AuthServiceFactory } from "./auth-service.factory";
import * as connectionManagerFactory from "./connection-manager.factory";
import { googleServiceFactory } from "./google-service.factory";
import { logFactory } from "./log.factory";
import { mailServiceFactory } from "./mail-service.factory";

// Controllers
import { AuthController } from "../controllers/auth.controller";
import { RouterController } from "../controllers/router.controller";
import { UserController } from "../controllers/user.controller";
import { VersionController } from "../controllers/version.controller";

// Router
import { router } from "../router";
import { ejsModuleFactory } from "./ejs-module.factory";

/**
 * Method for creating server handlers
 * @param envService Envservice to use
 * @param env Environment of application
 * @param apiHost Hostname of API
 */
export const handlerFactory = async (envService: EnvService, apiHost: string): Promise<handlerMethod[]> => {
  const env = envService.get("ENV", "local");

  // Get token from envService
  const token = envService.get("TOKEN", Math.random().toString());

  // Create log
  const log: LogModule = logFactory("server", envService);

  // Connect to DB
  const connection = await connectionManagerFactory.connect(
    ["production", "staging", "development"].includes(env) === true ?
      connectionManagerFactory.production(envService, logHandler(log), false) :
      connectionManagerFactory.local(logHandler(log), true));

  // Create services
  const hashtiService = new HashtiService();
  const claimService = new ClaimService(connection.getRepository(ClaimEntity));
  const groupService = new GroupService(connection.getRepository(GroupEntity), claimService);
  const bannedTokenService = new BannedTokenService(connection.getRepository(BannedTokenEntity));
  const userService = new UserService(connection.getRepository(UserEntity), claimService, groupService, {
    Brackets: typeorm.Brackets,
    IsNull: typeorm.IsNull,
    Not: typeorm.Not,
  });
  const jwtService = new JWTService(token);
  const mailService = mailServiceFactory(envService, ejsModuleFactory(envService));
  const authService = AuthServiceFactory(envService, bannedTokenService, userService, jwtService, hashtiService);
  const googleService = googleServiceFactory(envService);

  // Handlers
  return [vhost(
    apiHost,
    cors("*", undefined, ["Content-Type", "Authorization", "recaptcha"]),
    bodyParse(),
    router(
      new MiddlewareController(
        authService,
        googleService,
        token,
        log,
        envService.get("DEBUG", "") === "true" ? true : undefined
      ),
      new VersionController(new PackageJsonReaderModule(), log),
      new AuthController(
        authService,
        userService,
        hashtiService,
        googleService,
        mailService,
        log,
        envService.get("DEBUG", "") === "true" ? true : undefined
      ),
      new RouterController(log),
      new UserController(userService, log),
    ),
  )];
}
