// Libs
import VueRouter from "vue-router";

// Components
import ErrorComponent from "./components/error.component.vue";
import ExampleComponent from "./components/example.component.vue";
import ExampleBarComponent from "./components/example/example-bar.component.vue";
import ExampleButtonComponent from "./components/example/example-button.component.vue";
import ExampleCheckboxComponent from "./components/example/example-checkbox.component.vue";
import HomeComponent from "./components/home.component.vue";
import LoginComponent from "./components/login.component.vue";
import PasswordResetComponent from "./components/password-reset.component.vue";
import ResetPasswordComponent from "./components/reset-password.component.vue";

// Bootstrap
import { bootstrap } from "./bootstrap";

export const router = new VueRouter({
  mode: "history",
  routes: [
    { path: "/home", name: "home", component: HomeComponent },
    {
      path: "/examples", name: "example", component: ExampleComponent,
      children: [
        { path: "button", name: "example.button", component: ExampleButtonComponent },
        { path: "bar", name: "example.bar", component: ExampleBarComponent },
        { path: "checkbox", name: "example.checkbox", component: ExampleCheckboxComponent },
      ]
    },
    { path: "/login", name: "auth.login", component: LoginComponent, beforeEnter: bootstrap.middleware.guard([bootstrap.middleware.guest]) },
    { path: "/logout", name: "auth.logout", beforeEnter: bootstrap.middleware.guard([bootstrap.middleware.logout]) },
    { path: "/password-reset", name: "auth.password_reset", component: PasswordResetComponent, beforeEnter: bootstrap.middleware.guard([bootstrap.middleware.guest]) },
    { path: "/reset-password", name: "auth.reset_password", component: ResetPasswordComponent, beforeEnter: bootstrap.middleware.guard([bootstrap.middleware.guest]) },
    { path: "/error/:code", name: "error", component: ErrorComponent, beforeEnter: bootstrap.middleware.guard([bootstrap.middleware.errorCode]) },
    { path: "*", beforeEnter: bootstrap.middleware.guard([bootstrap.middleware.invalidRoute]) },
  ]
});