// Services
import { AuthService } from "../services/auth.service";
import { BroadcastService } from "../services/broadcast.service";
import { ErrorService } from "../services/error.service";
import { MenuService } from "../services/menu.service";
import { StorageService } from "../services/storage.service";

// Factories
import { apiServiceFactory } from "../../../shared/factories/api-service.factory";

// Middleware
import { Middlewares } from "../middlewares";

/* Main factory for SPA app. Modify this to return services (singletons)
for your app. Make sure to call this method only once to avoid multiple
instances of the same service */
export const SpaFactory = (): {
  authService: AuthService,
  menuService: MenuService,
  middleware: Middlewares,
} => {
  const broadcastService = new BroadcastService();
  const authService = new AuthService(
    apiServiceFactory(),
    new ErrorService(broadcastService),
    new StorageService(
      process.env.APP !== undefined ? process.env.APP : undefined,
      broadcastService,
    ),
  );
  return {
    authService,
    menuService: new MenuService(authService),
    middleware: new Middlewares(authService),
  }
}